"""
                 KEYBOARD - SCHEME
                 
            |       |       |       |
            |       |       |       |
Row 0 ----- 1 ----- 2 ----- 3 ----- A ---
            |       |       |       |
Row 1 ----- 4 ----- 5 ----- 6 ----- B ---
            |       |       |       |
Row 2 ----- 7 ----- 8 ----- 9 ----- C ---
            |       |       |       | 
Row 3 ----- * ----- 0 ----- # ----- D ---
            |       |       |       |
            |       |       |       |
            
         Column0 Column1 Column2 Column3
"""

import sys
import RPi.GPIO as GPIO
from pydub import AudioSegment
from pydub.playback import play

#Interrupts definition:
def IRQGPIO_32(i):
    print(i)
    print("You pressed: {}".format(vKeyBdMatrix[i][0]));
    play(AudioSegment.from_wav(vSoundsList[i][0]))
def IRQGPIO_36(i):
    print(i)
    print("You pressed: {}".format(vKeyBdMatrix[i][1]));
    play(AudioSegment.from_wav(vSoundsList[i][1]))
def IRQGPIO_38(i):
    print(i)
    print("You pressed: {}".format(vKeyBdMatrix[i][2]));
    play(AudioSegment.from_wav(vSoundsList[i][2]))
def IRQGPIO_40(i):
    print(i)
    print("You pressed: {}".format(vKeyBdMatrix[i][3]));
    play(AudioSegment.from_wav(vSoundsList[i][3]))

#Setting GPIO on Raspberry
GPIO.setmode(GPIO.BOARD);
GPIO.setwarnings(False);

#Pins number on Raspberry Pi board:
vRows = {"Zero Row" : 29,
         "First Row" : 31,
         "Second Row": 33,
         "Third Row" : 35};
vColumns = {"Zero Column" : 32,
         "First Column" : 36,
         "Second Column": 38,
         "Third Column" : 40};
#Test:
vRowList = [29,31,33,35];
vColumnList = [32,36,38,40];

#Sounds:
vSoundsList = [['1.wav','2.wav','3.wav','4.wav'],
               ['5.wav','6.wav','7.wav','8.wav'],
               ['9.wav','10.wav','11.wav','12.wav'],
               ['13.wav','14.wav','15.wav','16.wav']];

#Definition of keyboard
vKeyBdMatrix = [[1,2,3,'A'],
               [4,5,6,'B'],
               [7,8,9,'C'],
               ['*',0,'#','D']]

#Setting rows as outputss and setting 1 as default
for i in range(4):
    GPIO.setup(vRowList[i],GPIO.OUT);
    GPIO.output(vRowList[i],1);
    #print(k)
    
#Setting coulumns as inputs:
for i,k in vColumns.items():
    GPIO.setup(int(k),GPIO.IN,pull_up_down = GPIO.PUD_UP);
    #print(k)

#Interrupts:
"""
GPIO.add_event_detect(32,GPIO.FALLING,callback = IRQGPIO_32,bouncetime = 300);
GPIO.add_event_detect(36,GPIO.FALLING,callback = IRQGPIO_36,bouncetime = 300);
GPIO.add_event_detect(38,GPIO.FALLING,callback = IRQGPIO_38,bouncetime = 300);
GPIO.add_event_detect(40,GPIO.FALLING,callback = IRQGPIO_40,bouncetime = 300);
"""

#vCounter = 0;

#Reading pins:
#i - rows range, j - columns range
try:
    while(True):
        for i in range(4):
            GPIO.output(vRowList[i],0) #Active one pin
            for j in range(4):
                if GPIO.input(vColumnList[j]) == 0:
                    print("You pressed: {}".format(vKeyBdMatrix[i][j]));
                    play(AudioSegment.from_wav('/home/pi/Desktop/Radek_Projekt/Sounds/' + vSoundsList[i][j]))
                    while(GPIO.input(vColumnList[j]) == 0):
                        pass
            GPIO.output(vRowList[i],1);#inactive this pin
            #To interrupts
            #vCounter += 1
            #if(vCounter == 4):
                #vCounter = 0
    print("End of the loop");
#Cleaning interrupts from default closing and CTRL+C
except KeyboardInterrupt:
    GPIO.cleanup()
GPIO.cleanup()







