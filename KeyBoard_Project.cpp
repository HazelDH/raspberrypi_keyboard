#include <iostream>
#include <stdio.h>
#include <pigpio.h>
#include <typeinfo>
#include <string>
#include <stdlib.h>
#include <string.h>

/*
 *  Compilation programm:
 *  g++ -pthread -o Output KeyBoard_Project.cpp -lpigpio -lrt
 * 
 * 	Execution programm:
 * 	./raspberrypi_keyboard
 * 
 */
 
 
 //Playing sound
 void playSound(const char *filename ) {
    char command[256];

    /* create command to execute */
    sprintf( command, "aplay -c 1 -q -t wav ~/Desktop/Radek_Projekt2/Sounds/%s", filename );

    /* play sound */
    system( command );
}

 

int main()
{
	const short LOOP_ROW_RANGE = 4;
	const short LOOP_COLUMN_RANGE = 4;
	const int PIN_DOWN = 0;
	const int PIN_UP = 1;
	
	std::cout << "Test" << std::endl;
	
	//definition of rows and columns 
	int vRowList[] = {5,6,13,19}; // scaling - 29,31,33,35
	int vColumnList[] = {12,16,20,21};
	//Definition of keyboard
	const char vKeyBdMatrix[4][4] ={
		{'1','2','3','A'},
		{'4','5','6','B'},
		{'7','8','9','C'},
		{'*','0','#','D'}};
	
 	std::string vSoundsList[4][4] ={
		{"1.wav","2.wav","3.wav","4.wav"},
		{"5.wav","6.wav","7.wav","8.wav"},
		{"9.wav","10.wav","11.wav","12.wav"},
		{"13.wav","14.wav","15.wav","16.wav"}};
    
           
    gpioInitialise();        
	if(gpioInitialise() < 0) // < 0, sth went wrong
	{
		std::cout << gpioInitialise() << std::endl;
		return -1; 
	}
	
	//
	
	//std::cout << vKeyBdMatrix[2][3] << std::endl;
	//std::cout << vRowList[2] << std::endl;
	
	for(int i = 0; i < 4 ;i++)
	{
		gpioSetMode(vRowList[i],PI_OUTPUT); //setting Rows as outputs
		gpioWrite(vRowList[i],1); //Rows are "high" as default
		
		gpioSetMode(vColumnList[i],PI_INPUT); //setting columns as inputs
		gpioSetPullUpDown(vColumnList[i],PI_PUD_UP); //pull-ups resistors
	}
	
	while(true)
	{
		for(int i = 0; i < LOOP_ROW_RANGE;i++)
		{
			//std::cout << "i = " << i << std:: endl;
			gpioWrite(vRowList[i],0);
			for(int j = 0; j < LOOP_COLUMN_RANGE;j++)
			{
				//std::cout << "j = " << j << std:: endl;
				//std::cout << gpioRead(vColumnList[j]) << std::endl;
				if(gpioRead(vColumnList[j]) == 0)
				{
					std::cout << "You have pressed: " << vKeyBdMatrix[i][j] << std::endl;
					playSound(vSoundsList[i][j].c_str());
					//std::string vTemp = "canberra-gtk-play -f /Sounds/" + std::string(vSoundsList[i][j]);
					//system(vTemp.c_str()); //casting to const char *
					while(gpioRead(vColumnList[j]) == PIN_DOWN)
					{/*do nothing, write only the one letter at time*/}
				}
			}
			gpioWrite(vRowList[i],1);
		}
	}
	return 0;
}

